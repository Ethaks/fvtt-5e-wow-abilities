# World of Warcraft names for 5E abilities

This module for Foundry Virtual Tabletop changes the displayed names for abilities to the names used by the World of Warcraft setting.

## Installation

The module can be installed by pasting the following link into Foundry's module installation field:
[https://gitlab.com/Ethaks/fvtt-5e-wow-abilities/-/raw/master/module.json](https://gitlab.com/Ethaks/fvtt-5e-wow-abilities/-/raw/master/module.json)

## Legal

The software component of this system is distributed under the EUPL v. 1.2.
The terms of the [Foundry Virtual Tabletop End User License Agreement](https://foundryvtt.com/article/license/) apply.
