Hooks.on("ready", () => {
  // Create an object mirroring 5E's CONFIG, containing the values to be changed
  const wowConfig = {
    abilities: {
      con: "Stamina",
      dex: "Agility",
      wis: "Spirit",
    },
    abilityAbbreviations: {
      con: "Sta",
      dex: "Agi",
      wis: "Spi",
    },
  };

  // Merge the changes into 5E's config
  mergeObject(CONFIG.DND5E, wowConfig);

  // i18n
  const wowi18n = {
    AbilityCon: wowConfig.abilities.con,
    AbilityConAbbr: wowConfig.abilityAbbreviations.con,
    AbilityDex: wowConfig.abilities.dex,
    AbilityDexAbbr: wowConfig.abilityAbbreviations.dex,
    AbilityWis: wowConfig.abilities.wis,
    AbilityWisAbbr: wowConfig.abilityAbbreviations.wis,
  };
  // Merge translation strings
  mergeObject(game.i18n.translations.DND5E, wowi18n);

  console.log(`5E WoW Abilities | Finished loading`);
});
